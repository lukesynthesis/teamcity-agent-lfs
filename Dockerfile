# This creates a teamcity agent that also has git-lfs support.
FROM jetbrains/teamcity-agent
MAINTAINER Luke Machowski <luke@synthesis.co.za>

# Install git-lfs according to these instructions:
# https://github.com/github/git-lfs/blob/master/INSTALLING.md
# https://github.com/github/git-lfs

# Create the directory for the files we need to get git-lfs:
RUN mkdir -p /tmp/git-lfs && cd /tmp/git-lfs

# Download and execute the latest package for git-lfs:
RUN curl https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh -O && \
	chmod +x script.deb.sh && \
	./script.deb.sh && \
	apt-get install git-lfs && \
	git lfs install